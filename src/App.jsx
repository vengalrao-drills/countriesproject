import React, { useState, useEffect, useContext } from 'react'
import Header from './Components/Header.jsx'
import Search from './Components/Search.jsx'
import Display from './Components/Display.jsx'
import "./App.css"
import { useStorage } from './Context/toggleContext.jsx' 

const App = () => {
  const [countries, setCountries] = useState([]);
  const [region, setRegion] = useState('Filter Regions');
  const [search, setSearch] = useState('');
  const [population, setPopulation] = useState('normal');
  const [area, setArea] = useState('normal');
  const [selectSubRegion, setSelectSubRegion ] = useState('Filter by Subregions'); 
  const [error , setError] = useState(false);
  
  const { toggleTheme } = useStorage();
  useEffect(() => {
    fetch('https://restcountries.com/v3.1/all')
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network response was not ok.');
      })
      .then((data) => {
        setCountries(data);
      })
      .catch((error) => {
        setError(true);  
        console.error('Error fetching data:', error);  
      });
  }, []);

  const handleChange = (event) => {
    if (event.target.name == 'search') {
      setSearch(event.target.value);
    } else if (event.target.name == 'region') {
      setRegion(event.target.value);
      setPopulation('normal');
      setArea('normal');
      setSelectSubRegion('Filter by Subregions');
    } else if (event.target.name == 'population') {
      setPopulation(event.target.value);
      setArea('normal')
    } else if (event.target.name == 'area') {
      setArea(event.target.value); 
      setPopulation('normal');
    }else if(event.target.name == 'selectSubRegion'){
      setSelectSubRegion(event.target.value); 
    }
  }   

  return (
    <> 
      <div className={toggleTheme ? '' : 'light-dark-blue'}>
        <Header />
        <Search selectSubRegion={selectSubRegion} countries={countries}    region={region} search={search} handleChange={handleChange}  population={population} area={area}  />
        <Display error ={error}   selectSubRegion={selectSubRegion} countries={countries} search={search} region={region} population={population} area={area}    />
      </div>
    </>
  )
}
export default App