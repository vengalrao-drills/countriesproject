import React from 'react'
import { Route, Routes } from 'react-router-dom'
import App from './App'
import Country from './Components/Country'

const Routers = () => {
    return (
        <>
            <Routes>
                <Route path='/' element={<App />} />
                <Route path='/country/:cca3' element={<Country />} />
            </Routes>
        </>
    )
}

export default Routers
