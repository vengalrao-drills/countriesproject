import React from 'react';
import { useStorage } from '../Context/toggleContext';
import "../App.css";

const Select = ({ name, startingValue, handleChange, areaData, options, current }) => { 
  const { toggleTheme, changeToggleTheme } = useStorage();  
  return (
    <div> 
      <select name={name} value={current || startingValue} onChange={(e) => handleChange(e)} className={toggleTheme ? 'select' : 'select light-dark-blue'}>
        {options.map((option, index) => {
          return <option value={options[index]} key={index} >  {areaData[index]}  </option>
        })}
      </select>
    </div>
  )
}

export default Select
