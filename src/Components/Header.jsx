import React from 'react';
import "../App.css"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoon, faSun } from '@fortawesome/free-solid-svg-icons';
import { useStorage } from '../Context/toggleContext';
import { Link } from 'react-router-dom';

const Header = () => { 
  const { changeToggleTheme, toggleTheme } = useStorage(); 
  return (
    <div className={toggleTheme ? 'header light-gray ' : 'header light-blue'}>
     <h1 className='main-heading'>   <Link to='/' style={{ textDecoration: 'none', color:'inherit' }} >  Where in the World?  </Link>  </h1> 
      {toggleTheme ?
        <p onClick={() => changeToggleTheme()} className='mode' > <FontAwesomeIcon icon={faMoon} />  Dark Mode </p> :
        <p onClick={() => changeToggleTheme()} className='mode' > <FontAwesomeIcon icon={ faSun} />  Light Mode </p>}
    </div>
  )
}

export default Header
