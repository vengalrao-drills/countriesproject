import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { useStorage } from '../Context/toggleContext'

const Input = ({handleChange}) => {

    const { toggleTheme, changeToggleTheme } = useStorage();
    return (
        <div className={toggleTheme ? 's-firstDiv' : 's-firstDiv light-blue'}>
            <span><FontAwesomeIcon icon={faSearch} className={toggleTheme ? 'search-icon ' : 'search-icon light-blue'} /></span>
            <input type="text" name='search' className={toggleTheme ? 'countrySearch ' : 'countrySearch light-blue'} placeholder='Search for a country...' onChange={(e) => handleChange(e)} />
        </div>
    )
}

export default Input
