import React, { useEffect, useState } from 'react';
import "../App.css";
import { useStorage } from '../Context/toggleContext';
import Input from './Input';
import Select from './Select';


const Search = ({ selectSubRegion, region, handleChange, countries, population, area }) => {
  let subRegionData = [];
  const { toggleTheme, changeToggleTheme } = useStorage();
  let allRegions = [];
  countries.forEach((country, index) => {
    if (!allRegions.includes(country.region)) {
      allRegions.push(country.region);
    }
    if (country.region == region && !subRegionData.includes(country.subregion)) {
      subRegionData.push(country.subregion);
    }
  }) 

  let increDecreOption = ['normal', 'incre', 'decre'];
  let areaData = ['Sort by Area', 'Area Increasing Order', 'Area Decreasing Order'];

  let populationData = [' Sort by Population' , 'Population  Increasing Order', 'Population Decreasing Order '  ] ;

  if(allRegions){
    allRegions.unshift('Filter Regions'); 
  }

  if(subRegionData && allRegions){
    subRegionData.unshift('Filter by Subregions');
  } 
 
  return (
    <div className={toggleTheme ? ' search  ' : 'search   light-dark-blue'}>
      <Input handleChange={handleChange} /> 
      <Select  name={"area"} current={area} startingValue={"normal"} handleChange={handleChange}  options={increDecreOption} areaData={areaData} />
      <Select  name={"population"} current={population} startingValue={"normal"} handleChange={handleChange}  options={increDecreOption} areaData={populationData} />
      <Select  name={'region'} current={region} startingValue={'Filter Regions'} handleChange={handleChange} areaData={allRegions}  options={allRegions}   /> 
      {subRegionData.length > 1 ?
      <Select  name={'selectSubRegion'} current={selectSubRegion} startingValue={'Filter by Subregions'} handleChange={handleChange} areaData={subRegionData}  options={subRegionData} /> : ''}
    </div>
  )
}

export default Search
