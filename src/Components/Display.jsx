import React, { useContext, useEffect, useState } from "react";
import { Storage } from "../Context/toggleContext";
import { useNavigate } from "react-router-dom";

const Display = ({ error, countries, search, region, population, area, selectSubRegion }) => {
    const { toggleTheme } = useContext(Storage);
    let country = countries;
    if (countries) {
        country = countries
            .filter((country) => {
                if (search.length > 0) {
                    let name = country.name.common;
                    return name.toLowerCase().startsWith(search.toLowerCase());
                } else {
                    return true;
                }
            }).filter((country) => {
                if (region.toLowerCase() == "Filter Regions".toLowerCase()) {
                    return true;
                } else {
                    console.log('region  - -- - - ' , region , country.region)
                    if (country.region) {
                        if (country.region.toLowerCase().includes(region.toLowerCase())) {
                            return country.region.toLowerCase().includes(region.toLowerCase())
                        }
                    }
                }
            }).sort((c1, c2) => {
                if (area == 'decre') {
                    return -(c1.area - c2.area)
                } else if (area == 'incre') {
                    return c1.area - c2.area
                } else {
                    return 0
                }
            }).sort((c1, c2) => {
                if (population == 'incre') {
                    return c1.population - c2.population
                } else if (population == 'decre') {
                    return -(c1.population - c2.population)
                } else {
                    return 0
                }
            }).filter((country) => {
                if (selectSubRegion.toLowerCase() == "Filter by Subregions".toLowerCase()) {
                    return true;
                } else {
                    if (selectSubRegion == (country.subregion)) {
                        return true
                    } else {
                        return false
                    }
                }
            })
    }

    const navigate = useNavigate();
    const gotToCountry = (data) => {
        let c = data.cca3;
        navigate(`/country/${c}`, { state: { countries } })
    }

    return (
        <>
            {error ? <h1>404 Page ShutDown</h1> :
                <div className="display-Main">
                    <div className={toggleTheme ? "allCountries  " : "allCountries light-dark-blue"} >
                        {country.length > 0 ? (
                            country.map((country) => {
                                let language = "";
                                if (country.languages) { language = Object.values(country.languages).join(","); }
                                return (
                                    <div
                                        key={country.cca3}
                                        onClick={() => gotToCountry(country)}
                                        className={toggleTheme ? "country  " : "country  light-blue"}
                                    >
                                        <div className="c-info-left">
                                            <img className="image" src={country.flags.png} alt="" />
                                            <div className="matter">
                                                <h2 className="country-name"> {country.name.common}</h2>
                                                <p> <span className="side-head">Population: </span>  {country.population.toLocaleString()} </p>
                                                <p className="">  <span className="side-head">Region: </span>  {country.region}  </p>
                                                <p className="grow">  <span className="side-head"> Capital: </span> {country.capital}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        ) : (
                            <div className="matterr">
                                {countries && search.length == 0 ? (
                                    <>
                                        <h1>. . . . . . . Loading</h1>
                                        <p> <span className="circle"> </span></p>
                                    </>
                                ) : (<h1>  No such countries found :- <span className="span-country">{search}</span> </h1>)}
                            </div>
                        )}
                    </div>
                </div>}
        </>
    )
}
export default Display
