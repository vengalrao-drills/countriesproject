import React, { useContext, useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Header from './Header';
import { Storage } from '../Context/toggleContext';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';

const Country = () => {
  let { cca3 } = useParams();
  let { toggleTheme } = useContext(Storage);
  const [country, setCountry] = useState([]);
  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${cca3}`).then((response) => {
      return response.json();
    }).then((data) => {
      console.log(data)
      setCountry(data[0]);
    })
  }, [cca3]);

  return (
    <div className={toggleTheme ? "height-full" : " height-full light-dark-blue"}>
      <Header /> 
      <div className={toggleTheme ? 'p-country-main' : 'p-country-main  light-dark-blue'}  >
        <div>
          <button className={toggleTheme ? 'go-back light-border  light-gray ' : 'go-back blue-border  light-blue'}   > <Link to='/' style={{ textDecoration: 'none', color: 'inherit' }}>
            <FontAwesomeIcon icon={faArrowUp} className='icon-padd' /> Go Back </Link> </button>
        </div>
        {country?.name?.common ?
          <>
            <div className='particular-country  '>
              <div className="c-info-one">
                <img className="image" src={country.flags.png} alt="" />
              </div>
              <div className="p-country matter">
                <div>
                  <h2 className="country-name"> {country.name.common}</h2>
                </div>
                <div className="inner">
                  <div className='inner-1'>
                    <p>
                      <span className="side-head">Population: </span> {country.population.toLocaleString()} </p>
                    <p className="">
                      <span className="side-head">Region: </span> {country.region}
                    </p>
                    <p className="  ">
                      <span className=" side-head "> Subregion: </span>{country.subregion}
                    </p>
                    <p className="grow">
                      <span className="side-head"> Capital: </span>{country.capital}
                    </p>
                  </div>
                  <div className=" inner2    ">
                    <p><span className="side-head">Top level domain: </span> <span>{country.tld}</span></p>
                    {country.languages ? <>  <p > <span className='side-head'>Languages:</span>   <span> {Object.values(country.languages).join(',')} </span> </p>  </> : ""}
                    <p>
                      {
                        country.currencies ?
                          <> <span className='side-head'> Currencies: </span>
                            <span>
                              {(Object.values(country.currencies).reduce((accumulator, current) => {
                                accumulator.push(current.name)
                                return accumulator
                              }, [])).join(',')}
                            </span>
                          </> : ""
                      }
                    </p>
                  </div>
                </div>
                <div className="c-info-last matter">
                  <div>
                    {country.borders ? (
                      <>
                        <div>
                          {country.borders.length > 0 ? <span className="side-head">  Border Countries :  </span> : ''}
                          {country.borders && country.borders.map((border, index) => (
                            <span className="neighbours" key={index}>
                              <Link to={`/country/${border}`} style={{ textDecoration: 'none', color: 'inherit' }}>{border}</Link>
                            </span>
                          ))}
                        </div>
                      </>
                    ) : ("")}
                  </div>
                </div>
              </div>
            </div>
          </> : <>
            <div className="matter">
              <h1>. . . . . . . Loading <span className={toggleTheme ? "circle black" : "circle white "}> </span></h1>
            </div>
          </>
        }
      </div>
    </div>
  )
}

export default Country
