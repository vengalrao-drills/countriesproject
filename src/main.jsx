import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import Context from './Context/toggleContext.jsx'
import Routers from './Routers.jsx'
import { BrowserRouter } from 'react-router-dom'


ReactDOM.createRoot(document.getElementById('root')).render( 
    <Context  >
      <BrowserRouter> 
        <Routers />
      </BrowserRouter>
    </Context> 
)
