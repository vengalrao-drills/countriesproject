import React, { createContext, useContext, useState } from 'react'
const Storage = createContext(null);

const Context = ({ children }) => {
    const [toggleTheme, setToggleTheme] = useState(true);
    const changeToggleTheme = () => {
        setToggleTheme((prev) => (!prev))
    }
    return (
        <div>
            <Storage.Provider value={{ toggleTheme, changeToggleTheme }}>
                {children}
            </Storage.Provider>
        </div>
    )
}
export { Storage }
export default Context;

export const useStorage = () => {
    return useContext(Storage)
}
